const express=require('express');
const mongoose=require('mongoose');
const dotenv=require('dotenv').config();


const cors=require('cors');

const app=express();
const PORT = process.env.PORT || 4000;

// define the routes
const userRoutes=require('./Routes/userRoutes');
const productRoutes=require('./Routes/productRoutes');
const orderRoutes=require('./Routes/orderRoutes');

// middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

// connect db to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser:true, useUnifiedTopology:true});

// test the connection
mongoose.connection.on('error', console.error.bind(console, 'Connection Error'))
mongoose.connection.once('open', ()=> {console.log(`Connected to Database`)})

// root urls
app.use(`/capstone/users`, userRoutes);
app.use(`/capstone/products`, productRoutes);
app.use(`/capstone/orders`, orderRoutes);

app.listen(PORT, ()=>console.log(`Server connected to ${PORT}`))

