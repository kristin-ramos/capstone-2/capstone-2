
const express=require('express');
const router=express.Router();

// import function from controller
const {
    register,
    getAllUsers,
    updatePassword,
    updateUserInfo,
    deleteUser,
    login,
    profile,
	setAdmin,
	setUser,
	checkEmail
}=require('./../Controllers/userController')

const {verifyToken, decodeToken, verifyAdmin}= require('./../auth');

// register a user
router.post('/register', async (req, res)=> {
    try{
        await register(req.body).then(result=>res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})


// get all users
router.get('/', verifyAdmin, async (req, res) => {
	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// update user pw with token
router.patch('/updatepw', verifyToken, async (req, res)=>{
    
	const userId= decodeToken(req.headers.authorization).id
	try{
		await updatePassword(userId, req.body.password).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// update user info with token
router.put('/updateInfo', verifyToken, async (req, res)=>{

	const userId= decodeToken(req.headers.authorization).id
    try{
        await updateUserInfo(userId, req.body).then(result=>res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// delete user
router.delete('/delete', verifyAdmin, async (req, res)=>{
	try{
        await deleteUser(req.body).then(result=>res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// login user
router.post('/login', async (req, res)=>{
    
    try{
		await login(req.body).then(result=>res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



// retrieve user with token
router.get('/retrieve', verifyToken, async (req,res)=>{

	const userId = decodeToken(req.headers.authorization).id
	// console.log(userId)
	try{
		await profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// set user to admin with token
router.patch('/isAdmin', verifyAdmin, async (req, res)=>{

	try{
		await setAdmin(req.body).then(result => res.send(result))
	}
	catch(err){
		res.status(500).json(err)
	}
})

// set admin to user with token
router.patch('/isUser', verifyAdmin, async (req, res)=>{

	try{
		await setUser(req.body).then(result => res.send(result))
	}
	catch(err){
		res.status(500).json(err)
	}
})


// email exist
router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})


// export to connect to index
module.exports=router;