const express=require('express');
const router=express.Router();

// import function from controller
const {
    createProduct,
    getAllProducts,
    deleteProduct,
    updateProduct,
    activeProducts,
    inactiveProducts,
	setActiveStatus,
	setInactiveStatus,
	getOneProduct
}=require('./../Controllers/productController')

const {verifyToken, decodeToken, verifyAdmin}= require('./../auth');

// create product
router.post('/create', verifyAdmin, async (req, res)=>{
	try{
        await createProduct(req.body).then(result=>res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})


// view all products
router.get('/', verifyAdmin, async (req, res)=>{
	try{
        await getAllProducts().then(result=>res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// view one product
router.post('/search', verifyToken, async (req, res)=>{
	try{
        await getOneProduct(req.body).then(result=>res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})


// delete a product
router.delete('/deleteProduct', verifyAdmin, async (req, res)=>{
    // console.log('hi')
    try{
		await deleteProduct(req.body).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})

// update a product
router.patch('/update', verifyAdmin, async (req, res)=>{
    try{
		await updateProduct(req.body).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})

// get ACTIVE products
router.get('/isActive', verifyToken, async (req, res)=>{
    try{
		await activeProducts(req.body).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})


// get INACTIVE products
router.get('/isInactive', verifyAdmin, async (req, res)=>{
    try{
		await inactiveProducts(req.body).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})

// set product to ACTIVE
router.patch('/setActiveStatus', verifyAdmin, async (req, res)=>{
	try{
		await setActiveStatus(req.body).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})


// set product to inACTIVE
router.patch('/changeStatInact', verifyAdmin, async (req, res)=>{
	try{
		await setInactiveStatus(req.body).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})




// export to connect to index
module.exports=router;