const express=require('express');
const router=express.Router();

// import function from controller
const {
    orderProducts,
    retrieveOrders,
    deleteOrder,
    getUserOrders,
    orderNow
}=require('./../Controllers/OrderControllers')

const {verifyToken, decodeToken, verifyAdmin}= require('./../auth');

// order product
router.post('/order', verifyToken, async (req, res)=> {
    // console.log(req.body[0]._idOne)
    // console.log(req.body[1]._idTwo)
    // console.log(req.body)

    const email=decodeToken(req.headers.authorization)
    // console.log(email)
    await orderProducts(req.body, email).then(result=>res.send(result))

	// try{
    //     await orderProducts(req.body).then(result=>res.send(result))
    // }catch(err){
    //     res.status(500).json(err)
    // }
})




// get all orders
router.get('/orderslist', verifyAdmin, async (req, res)=> {
    try{
        await retrieveOrders().then(result=>res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
    
})


// delete orders
router.delete('/deleteOrder', verifyAdmin, async (req,res)=>{
    // console.log(req.body._id)
    await deleteOrder(req.body.id).then(result=>res.send(result))
})


// retrieve users order
router.get('/myOrder', verifyToken, async (req, res)=>{
    const email= decodeToken(req.headers.authorization)
    // console.log(email)
    await getUserOrders(email).then(result=>res.send(result))
})







// export to connect to index
module.exports=router;