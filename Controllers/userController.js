const User = require("../Models/User")
const CryptoJS=require('crypto-js');

//import createToken function from auth module
const {createToken} = require('./../auth');

// register a user
module.exports.register = async (reqBody) => {
    
    const {fullName, email, password, mobileNo, address}=reqBody

    return await User.findOne({email: reqBody.email}).then(result=>{
		// console.log(result)
		if(result !== null && result.email === reqBody.email){
			return {message: `Duplicate`}
		} else {
			if(result === null){
				
                let newUser =new User({
							fullName: fullName,
						    email: email,
						    password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString(),
							mobileNo: mobileNo,
							address: address
						})
				return newUser.save().then(result => result)
			}
		}
    })
}


// get all users
module.exports.getAllUsers = async () => {

	return await User.find().then(result => result)
}


// update user password
module.exports.updatePassword = async (userId, password) => {
	
	const newPw = {password : CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()}
	
	return await User.findByIdAndUpdate(userId, {$set: newPw}).then(result=>{
		if(result){
			result.password = "***"
			return result
		} else{
			return err
		}
	})
		
	
}

// update user info
module.exports.updateUserInfo = async (userId, reqBody)=>{
	
	const userInfo = {
		fullName: reqBody.fullName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		address: reqBody.address
	}
	return await User.findByIdAndUpdate(userId, {$set: userInfo}).then(result=>{
		if(result){
			result.password = "***"
			return {message: `succesfully updated`}
		} else{
			return err
		}
	})
}

// delete user
module.exports.deleteUser = async (reqBody) => {
	const {email} = reqBody
	return await User.deleteOne({email: reqBody.email}).then((result,err)=>{
		if(result){
			return {message: `user deleted`}
		}else{
			return {message: `user does not exist`}
		}
	})
}

// login user
module.exports.login = async (reqBody) => {
	const {email, password, mobileNo} = reqBody

	return await User.findOne({email: email}).then(result=>{
		console.log(result)
		if(result === null){
			return {message: `user does not exist`}
		}else{
			if(result !== null){
						
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
				if (password == decryptedPw){
					return {token: createToken(result)}
				}else{
					return {message: `auth failed`}
					}
		
			}else{
				return {message: `error`}
			}
		}
	})
}


// retrieve profile
module.exports.profile = async (userId) => {
	// console.log(result)	
	return await User.findById(userId).then(result=>result)
}

// set as admin
module.exports.setAdmin = async (reqBody) => {
	const {email} = reqBody
	return await User.findOneAndUpdate({email: reqBody.email},{$set :{isAdmin: true}},{new: true}).then(result=>{
		if(result){
			return result
		}else{
			return {message: `user does not exist`}
		}
	})
}

// set as user
module.exports.setUser = async (reqBody) => {
	const {email} = reqBody
	return await User.findOneAndUpdate({email: reqBody.email},{$set :{isAdmin: false}},{new: true}).then(result=>{
		if(result){
			return result
		}else{
			return {message: `user does not exist`}
		}
	})
}


// email exist
module.exports.checkEmail = async (reqBody) => {
	const {email} = reqBody

	return await User.findOne({email: email}).then((result, err) =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			} else {
				return err
			}
		}
	})
}