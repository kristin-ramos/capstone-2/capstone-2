const Product = require("../Models/Product")

// create a product
module.exports.createProduct = async (reqBody) => {
    const {name, description, colour, weight, price, stockQty}=reqBody
    return await Product.findOne({name: reqBody.name}).then((result,err)=>{
        if(result != null && result.name == reqBody.name){
            return {message: `product already exist`}
        }else{
            if(result == null){
                let newProduct = new Product ({
                    name: name,
                    description: description,
                    colour: colour,
                    weight: weight,
                    price: price,
                    stockQty: stockQty
                })
                return newProduct.save().then(result=>result)
            
            }
        }
    })
}

// get all products
module.exports.getAllProducts = async () => {
    return await Product.find().then(result=>result)
}

// view one product
module.exports.getOneProduct = async (reqBody) => {
    const {name} = reqBody
    return await Product.findOne({name: name}).then(result=>{

        // console.log(result)
        if(result){
            return result
        }else{
            return {message: `product does not exist`}
        }
    })
}

// delete products
module.exports.deleteProduct = async (reqBody) => {
    const {name} =reqBody
    return await Product.deleteOne({name: reqBody.name}).then((result, err) => result ? {message: `product is deleted`} : err)
}

// update a product
module.exports.updateProduct = async (reqBody) => {
        let updatedProd = {
            name: reqBody.name,
            description: reqBody.description,
            colour: reqBody.colour,
            weight: reqBody.weight,
            price: reqBody.price,
            stockQty: reqBody.stockQty
    }
    return await Product.findOneAndUpdate({name: reqBody.name}, {$set: updatedProd}, {new:true}).then(result=>{
        if(result){
			return result
		}else{
			return {message: `product does not exist`}
		}
    })
}

// get all ACTIVE products
module.exports.activeProducts = async () => {
    return await Product.find({isActive: true}).then(result=>result)
}

// get all INACTIVE products
module.exports.inactiveProducts = async () => {
    return await Product.find({isActive: false}).then(result=>result)
}

// set product to inACTIVE
module.exports.setInactiveStatus = async (reqBody) => {
    const {name} = reqBody
    return await Product.findOneAndUpdate({name: reqBody.name}, {$set: {isActive: false}}).then(result=>{
        
        if(result){
			return {message: `product status is now inActive`}
		}else{
			return {message: `product does not exist`}
		}
    })
}

// set product to ACTIVE
module.exports.setActiveStatus = async (reqBody) => {
    const {name} = reqBody
    return await Product.findOneAndUpdate({name: reqBody.name}, {$set: {isActive: true}}).then(result=>{
        if(result){
			return {message: `product status is now active`}
		}else{
			return {message: `product does not exist`}
		}
    })
}

// https://bit.ly/BootcamperSurvey

// https://careerkarma.com/review/zuitt/

// https://www.switchup.org/write-review

// https://www.coursereport.com/write-a-review