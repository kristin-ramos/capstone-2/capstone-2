const Order = require("../Models/Order")
const Product = require("../Models/Product")
const jwt=require('jsonwebtoken');
const {decodeToken}= require('./../auth');
const User = require("../Models/User");
const { response } = require("express");


// order product
module.exports.orderProducts = async (reqBody, headers) => {
    // const [{prodOne}, {prodTwo}] = reqBody
    const{ name, price, qty}=reqBody

    const{id, email, isAdmin}=headers
    // console.log(isAdmin)

    const userEmail= await User.findOne({email: email}).then(result=>result)
    // console.log(userEmail.email)  
    const uno = await Product.findOne({name: name}).then(result=>result)
    // console.log(uno)
    // const dos = await Product.findOne({name: nameTwo}).then(result=>result)
    // console.log(dos)
    // const tres = await Product.findOne({name: nameThree}).then(result=>result)
    // console.log(tres)
    
                if(isAdmin === true ){
                    // console.log(result)
                    return {message: `denied access`}   
                }
                else{
                    if(uno === null){
                        return {message: `product not existing`}
                    }else{
                        let first = new Order ({
                            // orderNo: orderNo,
                            email: email,
                            totalAmount: (price*qty),
                            
                            name : name,
                            price: price,
                            qty: qty,
                            amount: price*qty
                            
                            // prodTwo :{
                            //     nameTwo : nameTwo,
                            //     priceTwo: priceTwo,
                            //     qtyTwo: qtyTwo,
                            //     amountTwo: priceTwo*qtyTwo
                            // },
                            // prodThree :{
                            //     nameThree : nameThree,
                            //     priceThree: priceThree,
                            //     qtyThree: qtyThree,
                            //     amountThree: priceThree*qtyThree
                            // },                      
                        })
                        // console.log(first)
                        return first.save().then(result=>{
                            if(result){
                                return result
    
                            }else{
                                return {message: `error found`}
                            }
                        })
                    }

                }       
    } 







// get all orders by admin
module.exports.retrieveOrders = async () => {
    return await Order.find().then(result=>result)
}


// delete order
module.exports.deleteOrder = async (reqBody) => {
    return await Order.findOneAndDelete(reqBody).then(result=> {
        // console.log(result.email)
        if(result == null){
            return {message: `order is not existing`}
        }else{
            return {message: `order removed succesfully`}
        }
    })
}

// retrieve user order
module.exports.getUserOrders = async (headers) => {
    const {email} =headers
    return await Order.find({email: headers.email}).then(result=>{
        // console.log(result)
        if(result){
            return result
        }else{
            return {message: `no orders yet`}
        }
    })
}