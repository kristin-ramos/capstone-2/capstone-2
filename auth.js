const jwt=require('jsonwebtoken');

// create token
module.exports.createToken = (data) => {
    let userData = {
        id: data._id,
        email: data.email,
        isAdmin: data.isAdmin
    }
    return jwt.sign(userData, process.env.SECRET_PASS);
}

// decode token
module.exports.decodeToken = (extract) => {
    const token=extract.slice(7, extract.length)
    return jwt.decode(token)
}

// verify UserId token
module.exports.verifyToken = (req, res, next) => {
    const reqToken = req.headers.authorization
    // console.log(typeof reqToken)
 
    if(reqToken == undefined){
        return res.status(500).send({message: `missing token`})
    }else{
        const token = (req.headers.authorization).slice(7, req.headers.authorization.length)
        const userId = jwt.decode(token)
        
        if(userId !== undefined){ 
                  
            return jwt.verify(token, process.env.SECRET_PASS, (err, data)=>{
                
                if(err){
                    return res.send({message: `auth failed`})
                }else{
                    next()
                }
            })    
        }else{
            return res.send({message: `invalid user`})
        }
    }
}

// verify Admin token
module.exports.verifyAdmin = (req, res, next) => {
    const reqToken = req.headers.authorization
    // console.log(typeof reqToken)
 
    if(reqToken == undefined){
        return res.status(500).send({message: `missing token`})
    }else{
        const token = (req.headers.authorization).slice(7, req.headers.authorization.length)
        const admin = jwt.decode(token).isAdmin
        if(admin){
            return jwt.verify(token, process.env.SECRET_PASS, (err, data)=>{
                if(err){
                    return res.send({message: `auth failed`})
                }else{
                    next()
                }
            })
        }else{
            return res.send({message: `invalid user`})
        }
    }

    
}


