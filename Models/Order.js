const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
        // orderNo:{
        //     type: String,
        //     required: [true, `email is required`]           
        // },
        email:{
            type: String,
            required: [true, `email is required`]
        },
        totalAmount:{
            type:Number,
            required: [true, `totalAmount is required`]           
        },  
        name: {
            type: String,
            required: [true, `nameOne is required`]
        },      
        price:{
            type:Number,
            required: [true, `priceOne is required`]
        },
        qty:{
            type:Number,
            required: [true, `qtyOne is required`] 
        },
        amount:{
            type:Number, 
            required: [true, `amountOne is required`]           
        },
        // prodTwo:{
        //     nameTwo: {
        //         type: String,
                
        //     },      
        //     priceTwo:{
        //         type:Number,
                
        //     },
        //     qtyTwo:{
        //         type:Number,
                 
        //     },
        //     amountTwo:{
        //         type:Number,
                
        //     }                
        // },
        // prodThree:{
        //     productIdThree:{
        //         type:String,

        //     },
        //     nameThree: {
        //         type: String,

        //     },      
        //     priceThree:{
        //         type:Number,

        //     },
        //     qtyThree:{
        //         type:Number,

        //     },
        //     amountThree:{
        //         type:Number,

        //     }                
        // },
        orderedOn:{
            type:Date,
            default: new Date ()
        }
}, {timestamps: true}) 


// export the module
module.exports= mongoose.model(`Order`, orderSchema);