const mongoose=require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, `name required`]
    },
    description:{
        type: String,
        required: [true, `description required`]
    },
    colour:{
        type: String,
        required: [true, `color required`]     
    },
    weight:{
        type:String,
        default: [true, `weight is required`]
    },          
    price:{
        type:Number,
        default: [true, `Price is required`]
    },
    stockQty:{
        type:Number,
        default: [true, `Price is required`] 
    },
    isActive:{
        type:Boolean,
        default: true
    },
    createdOn:{
        type:Date,
        default: new Date ()
    },
    
}, {timestamps: true}) 


// export the module
module.exports= mongoose.model(`Product`, productSchema);