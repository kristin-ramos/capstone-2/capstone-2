const mongoose=require('mongoose');

const userSchema = new mongoose.Schema({
    fullName:{
        type: String,
        required: [true, `fullName required`]
    },
    email: {
        type: String,
        required: [true, `email required`]
    },
    password:{
        type: String,
        required: [true, `password required`]
    },
    mobileNo:{
        type: Number,
        required: [true, `mobileNo required`]
    },
    address:{
        type: String,
        required: [true, `address required`]
    },
    isAdmin:{
        type:Boolean,
        default: false
    }
}, {timestamps: true}) 


// export the module
module.exports= mongoose.model(`User`, userSchema);